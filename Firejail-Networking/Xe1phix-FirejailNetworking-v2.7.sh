#!/bin/sh
## Xe1phix-FirejailNetworking-v2.7.sh


firejail --net=eth0 --scan




## netfilter /usr/share/iptables/iptables.xslt
## netfilter /etc/iptables/rules.v4
## netfilter /etc/iptables/rules.v6
netfilter /etc/iptables/web-only.v4 
## netfilter /etc/iptables/web-only.v6

## 
## firejail --netfilter=/etc/firejail/nolocal.net
## firejail --netfilter.print=
## firejail --netfilter6.print=

firejail --net=eth0 --mac=00:11:22:33:44:55 firefox


sudo brctl addbr br0
sudo ifconfig br0 10.10.20.1/24
sudo brctl addbr br1
sudo ifconfig br1 10.10.30.1/24
firejail --net=br0 --net=br1



## ###################################### ## 
## ______ FrozenDNS _______
## nameserver 92.222.97.144
## nameserver 92.222.97.145
## 
## _______ OpenDNS _________
## nameserver 208.67.222.222
## nameserver 208.67.220.220
## ###################################### ## 
--ip=192.168.2.34 --dns=208.67.222.222
--ip=192.168.2.34 --dns=208.67.220.220

firejail --dns=8.8.8.8 --dns=8.8.4.4 firefox

firejail --dns.print=


# change netfilter configuration
sudo firejail --join-network=browser bash -c "cat /etc/firejail/nolocal.net | /sbin/iptables-restore"

# verify netfilter configuration
sudo firejail --join-network=browser /sbin/iptables -vL


--netfilter6=



firejail --netfilter.print=
firejail --netfilter6.print=



# verify  IP addresses
sudo firejail --join-network=browser ip addr


firejail --netfilter=/etc/firejail/webserver.net --net=eth0 /etc/init.d/apache2 start

firejail --netfilter=/etc/firejail/nolocal.net --net=eth0 firefox





firejail --protocol=unix

firejail --protocol=unix,inet
