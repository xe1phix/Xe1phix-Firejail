#!/bin/sh
## Xe1phix-Compiling-Firetools.sh

echo "##-============================-##"
echo "   [+] Install Depends:"
echo "##-============================-##"
apt-get install build-essential qt5-default qt5-qmake qtbase5-dev-tools libqt5svg5 git

echo "##-=============================================-##"
echo "     [+] Cloning The Firejail Github Repo...       "
echo "##-=============================================-##"
git clone  https://github.com/netblue30/firetools

echo "##-=====================================-##"
echo "     [+] Moving To That Directory...       "
echo "##-=====================================-##"
cd firetools

echo "##-=================================-##"
echo "     [+] Initiate Firejail Setup       "
echo "##-=================================-##"
./configure


echo "##-============================================================-##"
echo "     [+] Initiate Firejail Setup Using The Make Compiler...       "
echo "##-============================================================-##"
make
sudo make install-strip

